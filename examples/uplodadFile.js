const fs = require('fs');
const { WebClient } = require('@slack/client');
require('dotenv').load();
// An access token (from your Slack app or custom integration - xoxa, xoxp, or xoxb)
const token = process.env.SLACK_TOKEN;

const web = new WebClient(token);
console.log(token);
// Slack needs a file name for the upload
// This file is located in the current directory (`process.pwd()`)
const fileName = 'test_file.csv';
// See: https://api.slack.com/methods/files.upload
web.files.upload({
  fileName,
  // You can use a ReadableStream or a Buffer for the file option
  file: fs.createReadStream(`./${fileName}`),
  // Or you can use the content property (but not both)
  // content: 'plain string content that will be editable in Slack'
  // Specify channel(s) to upload the file to. Optional, unless also specifying a thread_ts value.
   channels: 'CEN616DDZ'
})
  .then((res) => {
    // `res` contains information about the uploaded file
    console.log('File uploaded: ', res.file.id);
  })
  .catch(console.error);