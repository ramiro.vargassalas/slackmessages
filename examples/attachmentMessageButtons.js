const { WebClient } = require('@slack/client');
require('dotenv').load();

// An access token (from your Slack app or custom integration - xoxa, xoxp, or xoxb)
const token = process.env.SLACK_TOKEN;
console.log(token);
const web = new WebClient(token);

// This argument can be a channel ID, a DM ID, a MPDM ID, or a group ID
const conversationId = 'CEN616DDZ';

web.chat.postMessage({
  channel: conversationId,
  "text": "Aproved Pull request?",
  "attachments": [
      {
          "text": "Choose a action",
          "fallback": "You are unable to choose a game",
          "callback_id": "wopr_game",
          "color": "#3AA3E3",
          "attachment_type": "default",
          "actions": [
              {
                  "name": "game",
                  "text": "Yes",
                  "type": "button",
                  "value": "chess"
              },
              {
                  "name": "game",
                  "text": "Need Work",
                  "type": "button",
                  "value": "maze"
              },
              {
                  "name": "game",
                  "text": "No",
                  "style": "danger",
                  "type": "button",
                  "value": "war",
                  "confirm": {
                      "title": "Are you sure?",
                      "text": "Wouldn't you prefer a good game of chess?",
                      "ok_text": "Yes",
                      "dismiss_text": "No"
                  }
              }
          ]
      }
  ]
})
  .then((res) => {
    // `res` contains information about the posted message
    console.log('Message sent: ', res.ts);
  })
  .catch(console.error);