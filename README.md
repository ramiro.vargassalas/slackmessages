# Slac Messages
Visit the [full documentation](https://slackapi.github.io/node-slack-sdk) for all the lovely details.


This package supports Node v6 and higher. It's highly recommended to use
[the latest LTS version of node](https://github.com/nodejs/Release#release-schedule), and the documentation is written
using syntax and features from that version.

## Installation

Use npm to install the package and save it to your `package.json`:

```shell
$ npm install @slack/client
```
Basic Message
Try this example with your own credential ,create .env file and replace the token variable
use node basicMessage.js to run the examples

```javascript
const { WebClient } = require('@slack/client');

// An access token (from your Slack app or custom integration - xoxa, xoxp, or xoxb)
const token = process.env.SLACK_TOKEN;

const web = new WebClient(token);

// This argument can be a channel ID, a DM ID, a MPDM ID, or a group ID
const conversationId = 'C1232456';

// See: https://api.slack.com/methods/chat.postMessage
web.chat.postMessage({ channel: conversationId, text: 'Hello there' })
  .then((res) => {
    // `res` contains information about the posted message
    console.log('Message sent: ', res.ts);
  })
  .catch(console.error);
```
You can find Token at the credentials of the App you need to create in slack API.
Use the Bot User OAuth Access token to send the message as the user of your own app
![picture](images/token.png)
you can find easily The conversationId at the url in the channel in slack for example here is `CEN616DDZ`
```
https://tcteamtest.slack.com/messages/CEN616DDZ/ 
```
## Posting a message with Incoming Webhooks

[Incoming webhooks](https://api.slack.com/incoming-webhooks) are an easy way to send notifications
to a Slack channel with a minimum of setup. You'll need a webhook URL from a Slack app or a custom
integration to use the `IncomingWebhook` class.

```javascript
const { IncomingWebhook } = require('@slack/client');
const url = process.env.SLACK_WEBHOOK_URL;
const webhook = new IncomingWebhook(url);

// Send simple text to the webhook channel
webhook.send('Hello there', function(err, res) {
    if (err) {
        console.log('Error:', err);
    } else {
        console.log('Message sent: ', res);
    }
});
```
You need to Create Incomming Webhook at the Api of your App in Slack ina specific Channel
![picture](images/webhook.png)
## Adding attachments to a message
[Adding attachments to a message](https://api.slack.com/docs/message-attachments) The chat.postMessage method takes an optional attachments argument. Arguments for Web API methods are all specified in a single object literal, so just add additional keys for any optional argument.
Also you can use Buttons with interactive message to create work flow and actions [Message with buttons](https://api.slack.com/tutorials/intro-to-message-buttons)
```javascript
web.chat.postMessage({
  channel: conversationId,
  text: 'Hello there',
  attachments: [
    {
      "fallback": "Required plain-text summary of the attachment.",
      "color": "#36a64f",
      "author_name": "Bobby Tables",
      "author_link": "http://flickr.com/bobby/",
      "author_icon": "http://flickr.com/icons/bobby.jpg",
      "title": "Slack API Documentation",
      "title_link": "https://api.slack.com/",
      "text": "Optional text that appears within the attachment",
      "fields": [
        {
          "title": "Priority",
          "value": "High",
          "short": false
        }
      ],
      "image_url": "http://my-website.com/path/to/image.jpg",
      "thumb_url": "http://example.com/path/to/thumb.png",
      "footer": "Slack API",
      "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
      "ts": 123456789
    }
  ]
})
  .then((res) => {
    // `res` contains information about the posted message
    console.log('Message sent: ', res.ts);
  })
  .catch(console.error);
  ```
## Uploading a file
The files.upload method can be used to upload a file, cool! This will require files:write:user scope.
```javascript
const fs = require('fs');
const { WebClient } = require('@slack/client');

// An access token (from your Slack app or custom integration - xoxa, xoxp, or xoxb)
const token = process.env.SLACK_TOKEN;

const web = new WebClient(token);

// Slack needs a file name for the upload
// This file is located in the current directory (`process.pwd()`)
const filename = 'test_file.csv';

// See: https://api.slack.com/methods/files.upload
web.files.upload({
  filename,
  // You can use a ReadableStream or a Buffer for the file option
  file: fs.createReadStream(`./${fileName}`),
  // Or you can use the content property (but not both)
  // content: 'plain string content that will be editable in Slack'
  // Specify channel(s) to upload the file to. Optional, unless also specifying a thread_ts value.
  // channels: 'C123456' * Specify here the channels you want to send the file uploaded
})
  .then((res) => {
    // `res` contains information about the uploaded file
    console.log('File uploaded: ', res.file.id);
  })
  .catch(console.error);
  ```
Here is how Look like the message we send through slack API 
![picture](images/result.png)